all:
	$(MAKE) clean -C dwm
	$(MAKE) -C dwm
	$(MAKE) clean -C st
	$(MAKE) -C st
	$(MAKE) clean -C slstatus
	$(MAKE) -C slstatus
	$(MAKE) clean -C dmenu
	$(MAKE) -C dmenu

install:
	$(MAKE) install -C dwm
	$(MAKE) install -C st
	$(MAKE) install -C slstatus
	$(MAKE) install -C dmenu
	cp ./dwm/dwm.desktop /usr/share/xsessions/
