/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Mononoki Nerd Font:size=12" };
static const char dmenufont[]       = "Mononoki Nerd Font:size=10";
static const char col_background[]  = "#282a36";
static const char col_line[]        = "#44475a";
static const char col_selection[]   = "#44475a";
static const char col_foreground[]  = "#f8f8f2";
static const char col_comment[]     = "#6272a4";
static const char col_cyan[]        = "#8be9fd";
static const char col_green[]       = "#50fa7b";
static const char col_orange[]      = "#ffb86c";
static const char col_pink[]        = "#ff79c6";
static const char col_purple[]      = "#bd93f9";
static const char col_red[]         = "#ff5555";
static const char col_yellow[]      = "#f1fa8c";
static const char *colors[][3]      = {
	/*               fg               bg              border   */
	[SchemeNorm] = { col_foreground, col_background, col_pink },
	[SchemeSel]  = { col_foreground, col_purple,  col_pink  },
};

static const char *const autostart[] = {
//    "setxkbmap", "-layout", "cz", "-variant", "qwerty", NULL,   // Seting keyboard layout to czech/qwerty
    "picom", NULL,
    "dunst", NULL,
    "slstatus", NULL,
    "feh", "--bg-fill", "/home/tuzu/stuff/Pictures/walls/wallpapers-dt/0016.jpg", NULL,
    NULL
};

typedef struct {
       const char *name;
       const void *cmd;
} Sp;
const char *spcmd1[] = {"alacritty", "--class", "sterm", NULL };
const char *spcmd2[] = {"thunar","--class", "sthunar", NULL };
const char *spcmd3[] = {"keepassxc", NULL };
static Sp scratchpads[] = {
       /* name          cmd  */
       {"sterm",          spcmd1},
       {"sthunar",        spcmd2},
       {"skeepassxc",     spcmd3},
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "ﱮ", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                  instance    title       tags mask     isfloating   monitor */
//	{ "Minecraft Launcher",   NULL,       NULL,       8,            1,           -1 },
      { NULL,           "sterm",               NULL,           SPTAG(0),               1,                           -1 },
      { NULL,           "sthunar",             NULL,           SPTAG(1),               1,                           -1 },
      { NULL,           "skeepassxc",          NULL,           SPTAG(2),               0,                           -1 },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "alacritty", NULL };
static const char *dmhub[]    = { "dm-hub", NULL };

#include <X11/XF86keysym.h>
static Key keys[] = {
	/* modifier                     key        function        argument */
	// Apps shortcuts
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,		XK_d,	   spawn,	   {.v = dmhub } },
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
        { MODKEY,                       XK_t,      togglescratch,  {.ui = 0 } },
        { MODKEY,                       XK_u,      togglescratch,  {.ui = 1 } },
        { MODKEY,                       XK_p,      togglescratch,  {.ui = 2 } },

	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },

	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

	{ MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	{ MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 10  } },

// Switch workspaces for us keyboard
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)


// Switch workspaces for cz keyboard
/*	TAGKEYS(                        XK_plus,                    0)
	TAGKEYS(                        XK_ecaron,                  1)
	TAGKEYS(                        XK_scaron,                  2)
	TAGKEYS(                        XK_ccaron,                  3)
	TAGKEYS(                        XK_rcaron,                  4)
	TAGKEYS(                        XK_zcaron,                  5)
	TAGKEYS(                        XK_yacute,                  6)
        TAGKEYS(                        XK_aacute,                  7)
	TAGKEYS(                        XK_iacute,                  8)
*/
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button1,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

